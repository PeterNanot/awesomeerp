<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Transaction;
use Tests\TestCase;

class TransactionModelTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Transaction $transaction */
    protected $transaction;

    public function setUp(): void
    {
        parent::setUp();
        $this->transaction = Transaction::factory()->create();
    }
    
    /** @test */
    public function is_between_company_and_provider_is_fine()
    {
        $this->transaction->update(['transaction_type' => Transaction::COMPANY_PROVIDER_TYPE]);
        $this->transaction->refresh();

        $this->assertTrue($this->transaction->isBetweenCompanyAndProvider());

        $this->transaction->update(['transaction_type' => Transaction::CLIENT_COMPANY_TYPE]);
        $this->transaction->refresh();

        $this->assertFalse($this->transaction->isBetweenCompanyAndProvider());
    }
    
    /** @test */
    public function is_between_client_and_company_is_fine()
    {
        $this->transaction->update(['transaction_type' => Transaction::CLIENT_COMPANY_TYPE]);
        $this->transaction->refresh();

        $this->assertTrue($this->transaction->isBetweenClientAndCompany());

        $this->transaction->update(['transaction_type' => Transaction::COMPANY_PROVIDER_TYPE]);
        $this->transaction->refresh();

        $this->assertFalse($this->transaction->isBetweenClientAndCompany());
    }
}
