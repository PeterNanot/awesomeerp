<?php

namespace Tests\Unit;

use App\Exceptions\NotEnoughStockOfThisProductException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Product;
use App\Models\Transaction;
use Tests\TestCase;

class ProductModelTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Product $product */
    protected $product;

    public function setUp(): void
    {
        parent::setUp();
        $this->product = Product::factory()->create();
    }

    /** @test */
    public function price_with_tva_is_correct()
    {
        $basicPrice = 100;
        $taxRate = 5.5;
        $expectedResult = 105.5;

        $this->product->update([
            'price' => $basicPrice,
            'tax' => $taxRate,
        ]);
        $this->product->refresh();

        $this->assertEquals($expectedResult, $this->product->priceWithTVA());
    }

    /** @test */
    public function sell_quantity_is_fine()
    {
        $productQuantityToSell = 100;
        $updatedQuantity = 0;

        $this->product->update([
            'stock' => $productQuantityToSell
        ]);
        $this->product->refresh();

        $this->product->sellQuantity($productQuantityToSell);

        $this->assertEquals($updatedQuantity, $this->product->stock);
    }

    /** @test */
    public function sell_quantity_throw_exception()
    {
        $productQuantityToSell = 100;
        $availableTooLowQuantity = 50;

        $this->product->update([
            'stock' => $availableTooLowQuantity
        ]);
        $this->product->refresh();

        $this->expectException(NotEnoughStockOfThisProductException::class);
        $this->product->sellQuantity($productQuantityToSell);
    }
    
    /** @test */
    public function quantity_can_be_purchased_is_fine()
    {
        $startingStock = 100;
        $halfStartingStock = 50;

        $this->product->update(['stock' => $startingStock]);
        $this->product->refresh();

        // Should be able to purchase half
        $this->assertTrue($this->product->quantityCanBePurchased($halfStartingStock));

        $this->product->update(['stock' => $halfStartingStock]);
        $this->product->refresh();

        // Can't purchase double
        $this->assertFalse($this->product->quantityCanBePurchased($startingStock));
    }
}
