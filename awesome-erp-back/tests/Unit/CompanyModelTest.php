<?php

namespace Tests\Unit;

use App\Exceptions\NotEnoughStockOfThisProductException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Company;
use App\Models\CompanyProduct;
use App\Models\Product;
use Tests\TestCase;

class CompanyModelTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Company $company */
    protected $company;

    public function setUp(): void
    {
        parent::setUp();
        $this->company = Company::factory()->create();
    }

    /** @test */
    public function can_purchased_is_true()
    {
        $this->company->update(['balance_amount' => 100.00]);
        $this->company->refresh();

        $equalAmount = 100.00;

        $this->assertTrue($this->company->canPurchase($equalAmount));

        $inferiorAmount = 10.00;

        $this->assertTrue($this->company->canPurchase($inferiorAmount));
    }

    /** @test */
    public function can_purchased_is_false()
    {
        $this->company->update(['balance_amount' => 100.00]);
        $this->company->refresh();

        $superiorAmount = 101.00;

        $this->assertFalse($this->company->canPurchase($superiorAmount));
    }

    /** @test */
    public function purchase_product_is_fine()
    {
        $sameAmountOfStock = 100;

        $companyProduct = Product::factory()->create([
            'stock' => $sameAmountOfStock,
        ]);

        $this->company->purchaseProduct($companyProduct->id, $sameAmountOfStock);

        $this->assertDatabaseHas('company_product', ['company_id' => $this->company->id, 'product_id' => $companyProduct->id, 'stock' => $sameAmountOfStock]);
    }

    /** @test */
    public function purchase_product_throw_exception()
    {
        $littleStock = 10;
        $bigPurchase = 100;

        $companyProduct = Product::factory()->create([
            'stock' => $littleStock,
        ]);

        $this->expectException(NotEnoughStockOfThisProductException::class);
        $this->company->purchaseProduct($companyProduct->id, $bigPurchase);
    }

    /** @test */
    public function got_enough_stock_is_fine()
    {
        $startingStock = 100;
        $quantityPossibleToSell = 50;
        $quantityNotPossibleToSell = 150;

        $productToSell = Product::factory()->create();

        // Create stock listing for this company on this product
        CompanyProduct::factory()->create([
            'company_id' => $this->company->id,
            'product_id' => $productToSell->id,
            'stock' => $startingStock,
        ]);

        $this->assertTrue($this->company->gotEnoughStock($quantityPossibleToSell, $productToSell->id));

        $this->assertFalse($this->company->gotEnoughStock($quantityNotPossibleToSell, $productToSell->id));
    }
}
