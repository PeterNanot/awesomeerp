<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Product $product */
    protected $product;

    public function setUp(): void
    {
        parent::setUp();

        $this->product = Product::factory()->create();
    }

    /**
     * Check product listing
     *
     * @return void
     */
    public function test_user_can_see_product()
    {
        $this->get('/products')
            ->assertSuccessful()
            ->assertSee($this->product->name);
    }

    /**
     * Check product store
     *
     * @return void
     */
    public function test_admin_can_add_product()
    {
        $productData = [
            'name' => 'Clavier',
            'price' => 100,
            'tax' => 5.5,
            'stock' => 2000,
        ];
        
        $this->post('/admin/products/', $productData)
            ->assertSuccessful();

        $this->assertDatabaseHas('products', $productData);
    }

    /**
     * Check product update
     *
     * @return void
     */
    public function test_admin_can_update_product()
    {
        $productDataToUpdate = [
            'name' => 'Clavier',
            'price' => 100,
            'tax' => 5.5,
            'stock' => 2000,
        ];
        
        $this->patch('/admin/products/' . $this->product->id, $productDataToUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('products', $productDataToUpdate);
    }

    /**
     * Check product destroy
     *
     * @return void
     */
    public function test_admin_can_delete_product()
    {
        $this->delete('/admin/products/' . $this->product->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('products', ['id' => $this->product->id]);
    }
}
