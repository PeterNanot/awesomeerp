<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Employee $employee */
    protected $employee;

    public function setUp(): void
    {
        parent::setUp();

        $this->employee = Employee::factory()->create();
    }

    /**
     * Check employee listing
     *
     * @return void
     */
    public function test_user_can_see_employee()
    {
        $this->get('/employees')
            ->assertSuccessful()
            ->assertSee($this->employee->name);
    }

    /**
     * Check employee store
     *
     * @return void
     */
    public function test_admin_can_add_employee()
    {
        $employeeUser = User::factory()->create();

        $employeeData = [
            'user_id' => $employeeUser->id,
            'name' => 'Donald Trump',
            'country' => 'France',
            'birthday' => now(),
            'starting_day' => now(),
        ];
        
        $this->post('/admin/employees/', $employeeData)
            ->assertSuccessful();

        $this->assertDatabaseHas('employees', ['name' => 'Donald Trump']);
    }

    /**
     * Check employee update
     *
     * @return void
     */
    public function test_admin_can_update_employee()
    {
        $employeeUser = User::factory()->create();

        $employeeDataToUpdate = [
            'user_id' => $employeeUser->id,
            'name' => 'Donald Trump',
            'country' => 'France',
            'birthday' => now(),
            'starting_day' => now(),
        ];
        
        $this->patch('/admin/employees/' . $this->employee->id, $employeeDataToUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('employees', ['name' => 'Donald Trump']);
    }

    /**
     * Check employee destroy
     *
     * @return void
     */
    public function test_admin_can_delete_employee()
    {
        $this->delete('/admin/employees/' . $this->employee->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('employees', ['id' => $this->employee->id]);
    }
}
