<?php

namespace Tests\Feature;

use App\Models\Provider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProviderTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Provider $provider */
    protected $provider;

    public function setUp(): void
    {
        parent::setUp();

        $this->provider = Provider::factory()->create();
    }

    /**
     * Check provider listing
     *
     * @return void
     */
    public function test_user_can_see_provider()
    {
        $this->get('/providers')
            ->assertSuccessful()
            ->assertSee($this->provider->name);
    }

    /**
     * Check provider store
     *
     * @return void
     */
    public function test_admin_can_add_provider()
    {
        $providerData = [
            'name' => 'Chief Nanot',
            'address' => '23 chemin de l\'impasse',
            'country' => 'France',
        ];
        
        $this->post('/admin/providers/', $providerData)
            ->assertSuccessful();

        $this->assertDatabaseHas('providers', $providerData);
    }

    /**
     * Check provider update
     *
     * @return void
     */
    public function test_admin_can_update_provider()
    {
        $providerDataToUpdate = [
            'name' => 'Chief Nanot',
            'address' => '23 chemin de l\'impasse',
            'country' => 'France',
        ];
        
        $this->patch('/admin/providers/' . $this->provider->id, $providerDataToUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('providers', $providerDataToUpdate);
    }

    /**
     * Check provider destroy
     *
     * @return void
     */
    public function test_admin_can_delete_provider()
    {
        $this->delete('/admin/providers/' . $this->provider->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('providers', ['id' => $this->provider->id]);
    }
}
