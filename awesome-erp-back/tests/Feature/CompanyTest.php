<?php

namespace Tests\Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Company $company */
    protected $company;

    public function setUp(): void
    {
        parent::setUp();

        $this->company = Company::factory()->create();
    }

    /**
     * Check company listing
     *
     * @return void
     */
    public function test_user_can_see_company()
    {
        $this->get('/companies')
            ->assertSuccessful()
            ->assertSee($this->company->name);
    }

    /**
     * Check company store
     *
     * @return void
     */
    public function test_admin_can_add_company()
    {
        $companyData = [
            'name' => 'Nanot Corp',
            'balance_amount' => 1000.50,
            'country' => 'France',
        ];
        
        $this->post('/admin/companies/', $companyData)
            ->assertSuccessful();

        $this->assertDatabaseHas('companies', $companyData);
    }

    /**
     * Check company update
     *
     * @return void
     */
    public function test_admin_can_update_company()
    {
        $companyDataToUpdate = [
            'name' => 'Nanot Corp',
            'balance_amount' => 1000.50,
            'country' => 'France',
        ];
        
        $this->patch('/admin/companies/' . $this->company->id, $companyDataToUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('companies', $companyDataToUpdate);
    }

    /**
     * Check company destroy
     *
     * @return void
     */
    public function test_admin_can_delete_company()
    {
        $this->delete('/admin/companies/' . $this->company->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('companies', ['id' => $this->company->id]);
    }
}
