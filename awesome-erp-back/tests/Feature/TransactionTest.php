<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\CompanyProduct;
use App\Models\Employee;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Employee $transaction */
    protected $transaction;

    public function setUp(): void
    {
        parent::setUp();

        $this->transaction = Transaction::factory()->create();
    }

    /**
     * Check transaction listing
     *
     * @return void
     */
    public function test_user_can_see_transaction()
    {
        $this->get('/transactions')
            ->assertSuccessful()
            ->assertSee($this->transaction->name);
    }

    /**
     * Check transaction store
     *
     * @return void
     */
    public function test_admin_can_add_transaction()
    {
        $amountToPurchase = 100.00;

        $transactionCompany = Company::factory()->create([
            'balance_amount' => $amountToPurchase,
        ]);

        $transactionProduct = Product::factory()->create();

        $transactionEmployee = Employee::factory()->create();

        $transactionData = [
            'transaction_amount' => $amountToPurchase,
            'transaction_type' => 0,
            'buyer_id' => $transactionCompany->id,
            'seller_id' => 1,
            'product_quantity' => 10,
            'product_id' => $transactionProduct->id,
            'employee_id' => $transactionEmployee->id,
        ];

        $this->post('/admin/transactions/', $transactionData)
            ->assertSuccessful();

        $this->assertDatabaseHas('transactions', $transactionData);
    }

    /**
     * Check transaction store
     *
     * @return void
     */
    public function test_admin_cant_add_transaction_because_not_enough_founds()
    {
        $amountToPurchase = 100.00;
        $notEnoughFounds = 50.00;

        $transactionCompany = Company::factory()->create([
            'balance_amount' => $notEnoughFounds,
        ]);

        $transactionProduct = Product::factory()->create();

        $transactionEmployee = Employee::factory()->create();

        $transactionData = [
            'transaction_amount' => $amountToPurchase,
            'transaction_type' => Transaction::COMPANY_PROVIDER_TYPE,
            'buyer_id' => $transactionCompany->id,
            'seller_id' => 1,
            'product_quantity' => 10,
            'product_id' => $transactionProduct->id,
            'employee_id' => $transactionEmployee->id,
        ];

        $this->post('/admin/transactions/', $transactionData);

        $this->assertDatabaseMissing('transactions', $transactionData);
    }

    /**
     * Check transaction store
     *
     * @return void
     */
    public function test_admin_cant_add_transaction_because_not_enough_stocks()
    {
        $amountToPurchase = 100.00;
        $companyStockNotEnough = 42;
        $productQuantity = 100;

        $transactionCompany = Company::factory()->create([
            'balance_amount' => $amountToPurchase,
        ]);

        $transactionProduct = Product::factory()->create();

        CompanyProduct::factory()->create([
            'company_id' => $transactionCompany->id,
            'product_id' => $transactionProduct->id,
            'stock' => $companyStockNotEnough,
        ]);

        $transactionEmployee = Employee::factory()->create();

        $transactionData = [
            'transaction_amount' => $amountToPurchase,
            'transaction_type' => Transaction::CLIENT_COMPANY_TYPE,
            'buyer_id' => $transactionCompany->id,
            'seller_id' => 1,
            'product_quantity' => $productQuantity,
            'product_id' => $transactionProduct->id,
            'employee_id' => $transactionEmployee->id,
        ];

        $this->post('/admin/transactions/', $transactionData);

        $this->assertDatabaseMissing('transactions', $transactionData);
    }

    /**
     * Check transaction update
     *
     * @return void
     */
    public function test_admin_can_update_transaction()
    {
        $amountToPurchase = 100.00;

        $transactionCompany = Company::factory()->create([
            'balance_amount' => $amountToPurchase,
        ]);

        $transactionProduct = Product::factory()->create();

        $transactionEmployee = Employee::factory()->create();

        $transactionDataToUpdate = [
            'transaction_amount' => $amountToPurchase,
            'transaction_type' => 0,
            'buyer_id' => $transactionCompany->id,
            'seller_id' => 1,
            'product_quantity' => 10,
            'product_id' => $transactionProduct->id,
            'employee_id' => $transactionEmployee->id,
        ];

        $this->patch('/admin/transactions/' . $this->transaction->id, $transactionDataToUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('transactions', $transactionDataToUpdate);
    }

    /**
     * Check transaction destroy
     *
     * @return void
     */
    public function test_admin_can_delete_transaction()
    {
        $this->delete('/admin/transactions/' . $this->transaction->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('transactions', ['id' => $this->transaction->id]);
    }
}
