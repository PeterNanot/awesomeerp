<?php

namespace Tests\Feature;

use App\Models\Client;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ClientTest extends TestCase
{
    use RefreshDatabase;

    /** @var \App\Models\Client $client */
    protected $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = Client::factory()->create();
    }

    /**
     * Check client listing
     *
     * @return void
     */
    public function test_user_can_see_client()
    {
        $this->get('clients')
            ->assertSuccessful()
            ->assertSee($this->client->name);
    }

    /**
     * Check client store
     *
     * @return void
     */
    public function test_admin_can_add_client()
    {
        $clientUser = User::factory()->create();

        $clientData = [
            'user_id' => $clientUser->id,
            'name' => 'Peter Nanot',
            'address' => '23 chemin de l\'impasse',
            'country' => 'France',
        ];
        
        $this->post('/admin/clients/', $clientData)
            ->assertSuccessful();

        $this->assertDatabaseHas('clients', $clientData);
    }

    /**
     * Check client update
     *
     * @return void
     */
    public function test_admin_can_update_client()
    {
        $clientUser = User::factory()->create();

        $clientDataToUpdate = [
            'user_id' => $clientUser->id,
            'name' => 'Peter Nanot',
            'address' => '23 chemin de l\'impasse',
            'country' => 'France',
        ];
        
        $this->patch('/admin/clients/' . $this->client->id, $clientDataToUpdate)
            ->assertSuccessful();

        $this->assertDatabaseHas('clients', $clientDataToUpdate);
    }

    /**
     * Check client destroy
     *
     * @return void
     */
    public function test_admin_can_delete_client()
    {
        $this->delete('/admin/clients/' . $this->client->id)
            ->assertSuccessful();

        $this->assertDatabaseMissing('clients', ['id' => $this->client->id]);
    }
}
