<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployeeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'name' => 'required',
            'country' => 'required',
            'birthday' => 'required',
            'starting_day' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => __("User id required."),
            'name.required' => __("Name required."),
            'country.required' => __("Country required."),
            'birthday.required' => __("Birthday required."),
            'starting_day.required' => __("Starting day required."),
        ];
    }
}
