<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'country' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => __("User id required."),
            'name.required' => __("Name required."),
            'address.required' => __("Address required."),
            'country.required' => __("Country required."),
        ];
    }
}
