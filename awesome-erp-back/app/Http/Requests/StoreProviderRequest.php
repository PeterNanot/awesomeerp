<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProviderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'country' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __("Name required."),
            'address.required' => __("Address required."),
            'country.required' => __("Country required."),
        ];
    }
}
