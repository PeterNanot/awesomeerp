<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',
            'tax' => 'required',
            'stock' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __("Name required."),
            'price.required' => __("Price required."),
            'tax.required' => __("Tax required."),
            'stock.required' => __("Stock required."),
        ];
    }
}
