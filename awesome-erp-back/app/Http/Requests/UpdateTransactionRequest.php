<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTransactionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_amount' => 'required',
            'transaction_type' => 'required',
            'buyer_id' => 'required',
            'seller_id' => 'required',
            'product_quantity' => 'required',
            'product_id' => 'required',
            'employee_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'transaction_amount.required' => __("Transaction amount required."),
            'transaction_type.required' => __("Transaction type required."),
            'buyer_id.required' => __("Buyer required."),
            'seller_id.required' => __("Seller required."),
            'product_quantity.required' => __("Product quantity required."),
            'product_id.required' => __("Product id required."),
            'employee_id.required' => __("Employee id required."),
        ];
    }
}
