<?php

namespace App\Http\Controllers\Admin;

use App\Models\Provider;
use App\Http\Requests\StoreProviderRequest;
use App\Http\Requests\UpdateProviderRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Product;

class AdminProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providersData = Provider::all();

        return $providersData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProviderRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProviderRequest $request)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        Provider::create($dataValidated);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function show(Provider $provider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function edit(Provider $provider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProviderRequest  $request
     * @param  \App\Models\Provider  $provider
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProviderRequest $request, int $providerId)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        $provider = Provider::find($providerId);

        $provider->update($dataValidated);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $providerId)
    {
        //Gate::authorize('admin-only');

        $provider = Provider::find($providerId);

        $provider->delete();
    }
}
