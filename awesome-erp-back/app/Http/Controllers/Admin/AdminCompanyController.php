<?php

namespace App\Http\Controllers\Admin;

use App\Models\Company;
use App\Http\Requests\StoreCompanyRequest;
use App\Http\Requests\UpdateCompanyRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class AdminCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companiesData = Company::all();

        return $companiesData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        Company::create($dataValidated);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCompanyRequest  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, int $companyId)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        $company= Company::find($companyId);

        $company->update($dataValidated);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $companyId)
    {
        //Gate::authorize('admin-only');

        $company = Company::find($companyId);

        $company->delete();
    }
}
