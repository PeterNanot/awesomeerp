<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CompanyCantPurchaseThisProductException;
use App\Exceptions\CompanyDontOwnEnoughStockOfThisProductException;
use App\Models\Transaction;
use App\Http\Requests\StoreTransactionRequest;
use App\Http\Requests\UpdateTransactionRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Product;

class AdminTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactionsData = Transaction::all();

        return $transactionsData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransactionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransactionRequest $request)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        if ($dataValidated["transaction_type"] === Transaction::COMPANY_PROVIDER_TYPE) {
            // Company buy to provider
            $buyerCompany = Company::find($dataValidated["buyer_id"]);

            // Check if company got enough money to buy product
            if (!$buyerCompany->canPurchase($dataValidated["transaction_amount"])) {
                throw new CompanyCantPurchaseThisProductException('This company can\'t afford this product, in this quantity.');
            } else {
                $buyerCompany->purchaseProduct($dataValidated['product_id'], $dataValidated['product_quantity']);
            }
        } else if ($dataValidated["transaction_type"] === Transaction::CLIENT_COMPANY_TYPE) {
            $sellerCompany = Company::find($dataValidated["seller_id"]);

            // Check if company got enough stock to sell product
            if (!$sellerCompany->gotEnoughStock($dataValidated["product_quantity"], $dataValidated["product_id"])) {
                throw new CompanyDontOwnEnoughStockOfThisProductException('This company do not have this product in this quantity.');
            } else {
                $product = Product::find($dataValidated['product_id']);

                $product->sellQuantity($dataValidated['product_quantity']);
            }
        }

        Transaction::create($dataValidated);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransactionRequest  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransactionRequest $request, int $transactionId)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        $transaction = Transaction::find($transactionId);

        $transaction->update($dataValidated);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $transactionId)
    {
        //Gate::authorize('admin-only');

        $transaction = Transaction::find($transactionId);

        $transaction->delete();
    }
}
