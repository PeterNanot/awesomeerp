<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Http\Requests\StoreClientRequest;
use App\Http\Requests\UpdateClientRequest;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;

class AdminClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientsData = Client::all();

        return $clientsData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreClientRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClientRequest $request)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        Client::create($dataValidated);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateClientRequest  $request
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClientRequest $request, int $clientId)
    {
        //Gate::authorize('admin-only');

        $dataValidated = $request->validated();

        $client = Client::find($clientId);

        $client->update($dataValidated);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $clientId)
    {
        //Gate::authorize('admin-only');

        $client = Client::find($clientId);

        $client->delete();
    }
}
