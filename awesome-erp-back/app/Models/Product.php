<?php

namespace App\Models;

use App\Exceptions\NotEnoughStockOfThisProductException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Transaction;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'tax',
        'stock',
    ];

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function priceWithTVA(): float
    {
        $taxAmount = ($this->price / 100.00) * $this->tax;

        return $this->price + $taxAmount;
    }

    public function sellQuantity(int $productQuantityToSell)
    {
        $updatedProductStock = $this->stock - $productQuantityToSell;

        if ($updatedProductStock < 0) {
            throw new NotEnoughStockOfThisProductException('Not enough quantity for this product.');
        }
        
        $this->update([
            'stock' => $updatedProductStock
        ]);
    }

    public function quantityCanBePurchased(int $quantity): bool
    {
        return $quantity <= $this->stock;
    }
}
