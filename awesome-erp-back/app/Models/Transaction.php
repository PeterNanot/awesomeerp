<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Employee;
use App\Models\Product;

class Transaction extends Model
{
    use HasFactory;

    public const COMPANY_PROVIDER_TYPE = 0;
    public const CLIENT_COMPANY_TYPE = 1;

    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'employee_id',
        'transaction_amount',
        'transaction_type',
        'buyer_id',
        'seller_id',
        'product_quantity',
        'stock',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function isBetweenCompanyAndProvider()
    {
        return $this->transaction_type === self::COMPANY_PROVIDER_TYPE;
    }

    public function isBetweenClientAndCompany()
    {
        return $this->transaction_type === self::CLIENT_COMPANY_TYPE;
    }
}
