<?php

namespace App\Models;

use App\Exceptions\NotEnoughStockOfThisProductException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'balance_amount',
        'country',
    ];

    public function canPurchase(float $amount): bool
    {
        return $this->balance_amount >= $amount;
    }

    public function purchaseProduct(int $productId, int $productQuantity)
    {
        $product = Product::find($productId);

        $updatedProductStock = $product->stock - $productQuantity;

        if ($productQuantity > $product->stock) {
            throw new NotEnoughStockOfThisProductException('Not enough quantity for this product.');
        }

        DB::transaction(function () use ($product, $productQuantity, $updatedProductStock) {
            //Create particular stock for company
            CompanyProduct::create([
                'company_id' => $this->id,
                'product_id' => $product->id,
                'stock' => $productQuantity
            ]);

            $product->update([
                'stock' => $updatedProductStock
            ]);
        });
    }

    public function gotEnoughStock(int $quantityToSell, int $productId): bool
    {
        $companyProductToSell = CompanyProduct::where([
            ['company_id', $this->id],
            ['product_id', $productId],
        ])->first();

        return $quantityToSell <= $companyProductToSell->stock;
    }
}
