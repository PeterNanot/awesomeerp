<?php

namespace App\Exceptions;

use Exception;

class CompanyCantPurchaseThisProductException extends Exception
{
    //
}
