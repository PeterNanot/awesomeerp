<?php

namespace App\Exceptions;

use Exception;

class CompanyDontOwnEnoughStockOfThisProductException extends Exception
{
    //
}
