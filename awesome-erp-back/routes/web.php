<?php

use App\Http\Controllers\Admin\AdminClientController;
use App\Http\Controllers\Admin\AdminCompanyController;
use App\Http\Controllers\Admin\AdminEmployeeController;
use App\Http\Controllers\Admin\AdminProductController;
use App\Http\Controllers\Admin\AdminProviderController;
use App\Http\Controllers\Admin\AdminTransactionController;

use App\Http\Controllers\ClientController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Admin
 */
Route::middleware([
    //'auth', 'admin', 
    'cors'
])
    ->prefix('admin')
    ->group(function () {

        // Clients Routing
        Route::post('clients', [AdminClientController::class, "store"]);

        Route::patch('clients/{id}', [AdminClientController::class, "update"]);

        Route::delete('clients/{id}', [AdminClientController::class, "destroy"]);

        // Employees Routing
        Route::post('employees', [AdminEmployeeController::class, "store"]);

        Route::patch('employees/{id}', [AdminEmployeeController::class, "update"]);

        Route::delete('employees/{id}', [AdminEmployeeController::class, "destroy"]);

        // Companies Routing
        Route::post('companies', [AdminCompanyController::class, "store"]);

        Route::patch('companies/{id}', [AdminCompanyController::class, "update"]);

        Route::delete('companies/{id}', [AdminCompanyController::class, "destroy"]);

        // Providers Routing
        Route::post('providers', [AdminProviderController::class, "store"]);

        Route::patch('providers/{id}', [AdminProviderController::class, "update"]);

        Route::delete('providers/{id}', [AdminProviderController::class, "destroy"]);

        // Products Routing
        Route::post('products', [AdminProductController::class, "store"]);

        Route::patch('products/{id}', [AdminProductController::class, "update"]);

        Route::delete('products/{id}', [AdminProductController::class, "destroy"]);

        // Transactions Routing
        Route::post('transactions', [AdminTransactionController::class, "store"]);

        Route::patch('transactions/{id}', [AdminTransactionController::class, "update"]);

        Route::delete('transactions/{id}', [AdminTransactionController::class, "destroy"]);
    });


/**
 * Public
 */
Route::middleware(['cors'])
    ->group(function () {
        Route::get('/', function () {
            return view('welcome');
        });

        // Client routing
        Route::get(
            'clients',
            [ClientController::class, 'index']
        );

        // Employee routing
        Route::get(
            'employees',
            [EmployeeController::class, 'index']
        );

        // Company routing
        Route::get(
            'companies',
            [CompanyController::class, 'index']
        );

        // Provider routing
        Route::get(
            'providers',
            [ProviderController::class, 'index']
        );

        // Product routing
        Route::get(
            'products',
            [ProductController::class, 'index']
        );

        // Transaction routing
        Route::get(
            'transactions',
            [TransactionController::class, 'index']
        );
    });
