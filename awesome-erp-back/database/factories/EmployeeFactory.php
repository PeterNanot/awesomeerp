<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{

    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $employeeUser = User::factory()->create();
        
        return [
            'user_id' => $attributes['user_id'] ?? $employeeUser->id,
            'name' => $attributes['name'] ?? $this->faker->name(),
            'country' => $attributes['country'] ?? $this->faker->country(),
            'birthday' => $attributes['birthday'] ?? now(),
            'starting_day' => $attributes['starting_day'] ?? now(),
        ];
    }
}
