<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => $attributes['user_id'] ?? function () {
                return User::factory()->create()->id;
            },
            'name' => $attributes['name'] ?? $this->faker->name(),
            'address' => $attributes['address'] ?? $this->faker->address(),
            'country' => $attributes['country'] ?? $this->faker->country(),
        ];
    }
}
