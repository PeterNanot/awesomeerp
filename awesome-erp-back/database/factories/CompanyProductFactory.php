<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\CompanyProduct;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyProductFactory extends Factory
{

    protected $model = CompanyProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'stock' => $attributes['stock'] ?? $this->faker->randomNumber(3),
            'company_id' => $attributes['company_id'] ?? function () {
                return Company::factory()->create()->id;
            },
            'product_id' => $attributes['product_id'] ?? function () {
                return Product::factory()->create()->id;
            },
        ];
    }
}
