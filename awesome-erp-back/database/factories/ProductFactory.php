<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $attributes['name'] ?? $this->faker->name(),
            'price' => $attributes['price'] ?? $this->faker->randomFloat(2, 0, 1000000),
            'tax' => $attributes['tax'] ?? $this->faker->randomFloat(2, 0, 50),
            'stock' => $attributes['stock'] ?? $this->faker->randomNumber(),
        ];
    }
}
