<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => $attributes['product_id'] ?? function () {
                return Product::factory()->create()->id;
            },
            'employee_id' => $attributes['employee_id'] ?? function () {
                return Employee::factory()->create()->id;
            },
            'transaction_amount' => $attributes['transaction_amount'] ?? $this->faker->randomFloat(2, 0, 1000000),
            'transaction_type' => $attributes['transaction_type'] ?? $this->faker->numberBetween(0, 1),
            'buyer_id' => $attributes['buyer_id'] ?? $this->faker->randomNumber(),
            'seller_id' => $attributes['seller_id'] ?? $this->faker->randomNumber(),
            'product_quantity' => $attributes['product_quantity'] ?? $this->faker->randomNumber(),
        ];
    }
}
