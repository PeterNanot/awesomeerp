<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProviderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $attributes['name'] ?? $this->faker->name(),
            'address' => $attributes['address'] ?? $this->faker->address(),
            'country' => $attributes['country'] ?? $this->faker->country(),
        ];
    }
}
