<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $attributes['name'] ?? $this->faker->name(),
            'balance_amount' => $attributes['balance_amount'] ?? $this->faker->randomFloat(2, 0, 1000000),
            'country' => $attributes['country'] ?? $this->faker->country(),
        ];
    }
}
