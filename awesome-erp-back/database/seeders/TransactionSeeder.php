<?php

namespace Database\Seeders;

use App\Models\Transaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions')->delete();

        $data = [
            [
                'transaction_amount' => 10.00,
                'transaction_type' => 0,
                'buyer_id' => 1,
                'seller_id' => 1,
                'product_quantity' => 10,
                'product_id' => 1,
                'employee_id' => 1,
            ],
            [
                'transaction_amount' => 15.00,
                'transaction_type' => 1,
                'buyer_id' => 1,
                'seller_id' => 1,
                'product_quantity' => 5,
                'product_id' => 1,
                'employee_id' => 1,
            ],
            [
                'transaction_amount' => 100.00,
                'transaction_type' => 0,
                'buyer_id' => 2,
                'seller_id' => 1,
                'product_quantity' => 30,
                'product_id' => 1,
                'employee_id' => 2,
            ],
            [
                'transaction_amount' => 50.00,
                'transaction_type' => 1,
                'buyer_id' => 1,
                'seller_id' => 2,
                'product_quantity' => 15,
                'product_id' => 4,
                'employee_id' => 3,
            ],
        ];

        Transaction::insert($data);
    }
}
