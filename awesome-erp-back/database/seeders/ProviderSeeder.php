<?php

namespace Database\Seeders;

use App\Models\Provider;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('providers')->delete();

        $data = [
            [
                'name' => 'Chief Nanot',
                'address' => '23 chemin de l\'impasse',
                'country' => 'France',
            ],
            [
                'name' => 'Awesome Provider',
                'address' => '23 chemin de l\'impasse',
                'country' => 'France',
            ],
        ];

        Provider::insert($data);
    }
}
