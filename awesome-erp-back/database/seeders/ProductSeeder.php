<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->delete();

        $data = [
            [
                'name' => 'Clavier',
                'price' => 100,
                'tax' => 5.5,
                'stock' => 2000,
            ],
            [
                'name' => 'souris',
                'price' => 50,
                'tax' => 5.5,
                'stock' => 2500,
            ],
        ];

        Product::insert($data);
    }
}
