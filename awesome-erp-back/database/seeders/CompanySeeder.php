<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();

        $data = [
            [
                'name' => 'Nanot Corp',
                'balance_amount' => 1000.50,
                'country' => 'France',
            ],
            [
                'name' => 'engIT',
                'balance_amount' => 1000.00,
                'country' => 'France',
            ],
        ];

        Company::insert($data);
    }
}
