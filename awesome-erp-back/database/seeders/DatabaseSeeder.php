<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        \App\Models\Client::factory(10)->create();
        \App\Models\Company::factory(10)->create();
        \App\Models\Employee::factory(10)->create();
        \App\Models\Provider::factory(10)->create();
        \App\Models\Product::factory(10)->create();
        \App\Models\Transaction::factory(10)->create();
        //$this->call(ClientSeeder::class);
        //$this->call(CompanySeeder::class);
        //$this->call(EmployeeSeeder::class);
        //$this->call(ProviderSeeder::class);
        //$this->call(ProductSeeder::class);
        //$this->call(TransactionSeeder::class);
    }
}
