<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->delete();

        $data = [
            [
                'user_id' => 4,
                'name' => 'Donald Trump',
                'country' => 'France',
                'birthday' => now(),
                'starting_day' => now(),
            ],
            [
                'user_id' => 5,
                'name' => 'Linus Torvalds',
                'country' => 'France',
                'birthday' => now(),
                'starting_day' => now(),
            ],
            [
                'user_id' => 6,
                'name' => 'Miley Cirus',
                'country' => 'France',
                'birthday' => now(),
                'starting_day' => now(),
            ],
        ];

        Employee::insert($data);
    }
}
