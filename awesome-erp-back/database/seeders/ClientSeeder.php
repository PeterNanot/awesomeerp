<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->delete();

        $data = [
            [
                'user_id' => 1,
                'name' => 'Peter Nanot',
                'address' => '23 chemin de l\'impasse',
                'country' => 'France',
            ],
        ];

        Client::insert($data);
    }
}
