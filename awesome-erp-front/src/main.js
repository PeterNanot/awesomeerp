import Vue from 'vue'
import AppEmployee from './AppEmployee.vue'

// import ClientListing from "./components/ClientListing";
// import EmployeeListing from "./components/EmployeeListing";
import AppClient from "./AppClient";
import AppProvider from "./AppProvider";
import AppProduct from "./AppProduct";
import AppCompany from "./AppCompany";
import AppTransaction from "./AppTransaction";

const routes = {
  '/client': AppClient,
  '/employee': AppEmployee,
  '/provider': AppProvider,
  '/product': AppProduct,
  '/company': AppCompany,
  '/transaction': AppTransaction,
}

Vue.config.productionTip = false

new Vue({
  el: '#app',
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] || AppEmployee
    }
  },
  render (h) { return h(this.ViewComponent) }
})