# Awesome-erp

## Project setup
```
docker-compose up --build -d
```

## Run back and front in local (optionnal)

Comment both container ('app' and 'awesome-erp-front') in docker-compose.yml file, launch :
```
docker-compose up --build -d
```

On directory 'awesome-erp-back', you'll have to modify .env, DB_HOST is now 'localhost', launch :

```
php artisan serve
php artisan migrate
php artisan db:seed
```

On directory 'awesome-erp-front', launch :

```
npm run serve
```

## Testing back

Launch test on 'app' container :
```
docker exec -it app php artisan test
```

Launch test from local directory :
```
php artisan test
```

## What's missing ?

- No authentication, project got two roles, public and admin, middleware and gate are in position, but I've comment them as they are not finish, so you can test front without problem
- Front needs some 'select' html element in forms, to select ids, without thoses select you need to know an id who already exist of this specific entity (not really user friendly)
- Rules that apply on creation do not on update, but it's basicaly the same code

## In trouble ?

If you got any problem with the setup, or something is not working properly, don't hesitate to contact me !